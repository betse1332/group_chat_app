package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
        
        //command line arguments for address and port
//        String address=this.getParameters().getRaw().get(0);
//        int port=Integer.parseInt(this.getParameters().getRaw().get(1));

        primaryStage.setTitle("Chat world");
        primaryStage.setScene(new Scene(root, 450, 650));
        primaryStage.show();

    }


    public static void main(String[] args) {
        launch(args);
    }
}
