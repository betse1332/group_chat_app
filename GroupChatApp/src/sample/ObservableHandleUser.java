package sample;



import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class ObservableHandleUser extends Observable implements Observer,Runnable {
    final DataOutputStream outputStream;
    final DataInputStream inputStream;
//    final ObjectOutputStream objectOutputStream;


    public String getUsername() {
        return username;
    }

    private String username;
    private Socket socket;

    public ObservableHandleUser(Socket socket) throws Exception {
        this.socket = socket;
        outputStream = new DataOutputStream(socket.getOutputStream());
        inputStream = new DataInputStream(socket.getInputStream());
//        objectOutputStream =new ObjectOutputStream(socket.getOutputStream());
    }

    private List<Observer> subscribers = new ArrayList<>();

    public void addMessage(String message) {
        notifyObservers(message);
    }

    public void notifyObservers(String message) {
        for (Observer subscriber : subscribers) {
            subscriber.update(this, message);
        }
    }

    public void registerObserver(Observer newSubscriber) {
        this.subscribers.add(newSubscriber);
    }

    public void removeObserver(Observer previousSubscriber) {
        this.subscribers.remove(previousSubscriber);
    }



    @Override
    public void update(Observable o, Object message) {
           try {
               outputStream.writeUTF((String)message );
           }catch (Exception e){
               e.printStackTrace();
           }

    }

    public void sendUsername(String userName){
        System.out.println("USER NAME TO SEND :"+ userName);
        try{
            outputStream.writeUTF(userName);
            outputStream.flush();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void run() {


        while (username==null) {
            try {
                username = inputStream.readUTF();
                System.out.println("username"+username);
                if (username != null) {
                    System.out.println("add username");
                    addMessage(username);
                    System.out.println("observers size"+ChatServer.observers.size());
                    ChatServer.observers.add(this);





                    ChatServer.clientsUsername.add(username);
//                    outputStream.writeObject(ChatServer.observers);
                    System.out.println("user :"+username+" has joined");

                }


            }catch(IOException e){
                ChatServer.observers.remove(this);
                System.out.println("Client:" + username+"left");
                ChatServer.clientsUsername.remove(username);
                break;
            }
        }
        while (true){

            try {
                String userInput=inputStream.readUTF();

                System.out.println(userInput+"user input from a server");
                if (userInput!=null ){
                    outputStream.writeUTF(username+ ": "+userInput);
                    addMessage(username+":"+userInput);
                }
                if (userInput.equals("Quit")){
                            ChatServer.observers.remove(this);
                            ChatServer.clientsUsername.remove(this.username);
                }



            }catch (Exception e){
                ChatServer.observers.remove(this);
                System.out.println("Client:" + username+"left");
                ChatServer.clientsUsername.remove(username);
                break;


            }
        }
    }
}
