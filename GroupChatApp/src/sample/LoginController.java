package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class LoginController {
    public static String clientMessage;
    public  static String toClient;
    private String clientUsername;

    @FXML
    private TextField address;

    @FXML
    private TextField port;

    @FXML
    private TextField username;
    public static ChatClient newClient;

    public void joinChat(ActionEvent actionEvent)  throws Exception{

            SingletonObservers clientNameObserver=SingletonObservers.getSingletonObservers();
            String hostAddress=address.getText();
            int hostPort= Integer.parseInt(port.getText());

            Stage stage=(Stage)username.getScene().getWindow();
            Parent root= FXMLLoader.load(getClass().getResource("sample.fxml"));



            clientUsername=username.getText();
            try {
                newClient=ChatClient.getInstance(hostPort,hostAddress,clientUsername);
                newClient.start();
                newClient.sendMessage(clientUsername);

//                clientNameObserver.setHostAddress(hostAddress);
//                clientNameObserver.setPort(hostPort);
//                clientNameObserver.setUsername(clientUsername);
                final Label usernameLabel=new Label(clientUsername);
                usernameLabel.setStyle("-fx-background-color:#2f2f2f;-fx-text-fill:a9a9a9;-fx-background-radius:5,4,3,5;-fx-font-size:15px");

                usernameLabel.setPadding(new Insets(10,10,10,10));
                 clientNameObserver.clientUsernameObserver.add(usernameLabel);



//                Controller.clientsObserver.add(newClient.getUsername());

                //Add username to username observer;




                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
            }catch (Exception e){
                e.printStackTrace();
            }
    }
}
