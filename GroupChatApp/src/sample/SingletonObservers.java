package sample;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;


import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class SingletonObservers extends Observable {
    private static   SingletonObservers SINGLETON_OBSERVER_INSTANCE=null;

    public ObservableList<Label> clientUsernameObserver ;
    public ObservableList<HBox> chatMessageObserver;
    private  String  clientMessage="";

    public String getUserName() {
        return userName;
    }

    private String userName="";

    private boolean logout=false;

    public boolean isLogout() {
        return logout;
    }




    public String getClientMessage() {

        return clientMessage;

    }

    public void setClientMessage(String clientMessage) {
        String[] logoutMessage=clientMessage.split(":");
        this.userName=logoutMessage[0];
        if (logoutMessage.length>=2 && logoutMessage[1].equals("Quit")){
            this.logout=true;
//            this.userName=logoutMessage[0];
        }
        this.clientMessage = clientMessage;
    }




    private  SingletonObservers(){
            clientUsernameObserver=FXCollections.observableArrayList();
            chatMessageObserver=FXCollections.observableArrayList();


    }



    public static synchronized   SingletonObservers getSingletonObservers() {

        if (SINGLETON_OBSERVER_INSTANCE==null){
            System.out.println("creating new singleton object once");
            SINGLETON_OBSERVER_INSTANCE=new SingletonObservers();
        }

        return SINGLETON_OBSERVER_INSTANCE;
    }




}
