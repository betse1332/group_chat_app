package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Observer;
import java.util.Scanner;
import java.util.Set;

public class ChatServer {


    private static   int port;
    public static Set<ObservableHandleUser> observers = new HashSet<>();
    public static Set<String> clientsUsername=new HashSet<>();


    public static void main(String[] args ){


        new Thread(()->{
            try{
                Scanner input=new Scanner(System.in);
                System.out.print("Enter server port:");
                port=input.nextInt();
                ServerSocket serverSocket=new ServerSocket(port);
                System.out.println("multithreaded server has started waiting for clients to join chat....");
                while (true){
                    Socket socket = serverSocket.accept();

                    ObservableHandleUser userHandler=new ObservableHandleUser(socket);
                        for (ObservableHandleUser stream:observers){
                        userHandler.registerObserver(stream);
                        stream.registerObserver(userHandler);
                        userHandler.sendUsername(stream.getUsername());

                    }
//                    for (String username:clientsUsername){
//                        System.out.println("Sending username"+ username);
//                        userHandler.sendUsername(username);
//                    }

                    new Thread(userHandler).start();
                }

            }catch (Exception  e){
                e.printStackTrace();
            }
        }).start();


    }
}
