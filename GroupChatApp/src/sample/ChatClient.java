package sample;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.io.*;
import java.net.Socket;
import java.util.Set;

public class ChatClient {
    private static  ChatClient chatClientInstance=null;
    private   int port;
    private   String address;
    private Socket clientSocket;
    private DataOutputStream  clientOutStream;
    private DataInputStream clientInStream;




    private Set<String> clientUsername;



    private String username;




    private ChatClient(int port,String address,String username) throws Exception{
        this.username=username;
        this.port=port;
        this.address=address;
        this.clientSocket=new Socket(address,port);
        clientInStream=new DataInputStream(clientSocket.getInputStream());
        clientOutStream=new DataOutputStream(clientSocket.getOutputStream());
//        objectInputStream=new ObjectInputStream(clientSocket.getInputStream());

    }
    public  void sendMessage(String message){
        if (message !=null){
            try {
                clientOutStream.writeUTF(message);

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    public String getUsername() {
        return username;
    }
    public void start(){
        SingletonObservers chatObserver=SingletonObservers.getSingletonObservers();
//        Thread messageSender=new Thread(new Runnable() {
//            @Override
//            public void run() {
//                while (true){
//                    try {
//
//                        if (LoginController.clientMessage !=null ){
////                            System.out.println(LoginController.clientMessage);
//                            clientOutStream.writeUTF(LoginController.clientMessage);
//                        }
//
//
//
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//                }
//            }
//        });

        Thread messageReceiver=new Thread(new Runnable() {

            @Override
            public void run() {
//                System.out.println("Receiver is not running" +LoginController.toClient);
               while (true){
                   try {
                       System.out.println("====="+LoginController.toClient);
                       chatObserver.setClientMessage(clientInStream.readUTF());
//                       LoginController.toClient=clientInStream.readUTF();
                       System.out.println("user  found");

//                       System.out.println("client message "+LoginController.toClient);


                       System.out.println("client message: "+chatObserver.getClientMessage());

                       if (chatObserver.getClientMessage()!=null && chatObserver.getClientMessage().contains(":") ){
                           System.out.println("Message received "+chatObserver.getClientMessage());

                           Platform.runLater(new Runnable() {
                               @Override
                               public void run() {
                                   if (chatObserver.isLogout()){
                                       System.out.println("user left:"+chatObserver.getUserName());
                                       chatObserver.clientUsernameObserver.remove(chatObserver.getUserName());
                                   }
                                        final Label messageLabel=new Label(chatObserver.getClientMessage());
                                      HBox messageBox;
                                     System.out.println(username+"::"+chatObserver.getUserName());
                                      Label emptyLabel=new Label();
                                        if (username.equals(chatObserver.getUserName())){
                                            messageBox=new HBox( 280);
                                          messageLabel.setStyle("-fx-background-color:#2f2f2f;-fx-text-fill:a9a9a9;-fx-background-radius:5,4,3,5;-fx-font-size:15px");

                                            messageLabel.setPadding(new Insets(10,10,10,10));


                                            messageBox.getChildren().addAll(emptyLabel,messageLabel);
                                        }else{
                                            messageBox=new HBox(5);
                                            messageLabel.setStyle("-fx-background-color:#8b4513;-fx-text-fill:a9a9a9;-fx-background-radius:5,4,3,5;-fx-font-size:15px");

                                            messageLabel.setPadding(new Insets(10,10,10,10));
                                            messageBox.getChildren().addAll(emptyLabel,messageLabel);

                                        }

                                        chatObserver.chatMessageObserver.add(messageBox);

                                      chatObserver.setClientMessage("");

                               }

                           });

                       }else if( chatObserver.getClientMessage()!=null && !chatObserver.getClientMessage().contains(":")){

                           String updateObserver=chatObserver.getClientMessage();
                           Platform.runLater(new Runnable() {
                               @Override
                               public void run() {
                                   final Label messageLabel =new Label(updateObserver);
                                   System.out.println("update username observer");

                                   if (username.equals(chatObserver.getUserName())){

                                       messageLabel.setStyle("-fx-background-color:#2f2f2f;-fx-text-fill:a9a9a9;-fx-background-radius:5,4,3,5;-fx-font-size:15px");

                                       messageLabel.setPadding(new Insets(10,10,10,10));



                                   }else{

                                       messageLabel.setStyle("-fx-background-color:#8b4513;-fx-text-fill:a9a9a9;-fx-background-radius:5,4,3,5;-fx-font-size:15px");

                                       messageLabel.setPadding(new Insets(10,10,10,10));


                                   }
                                   chatObserver.clientUsernameObserver.add(messageLabel);

                               }
                           });
                           chatObserver.setClientMessage("");

                       }
//                       Thread.sleep(2000);


                   } catch (Exception e) {
                       e.printStackTrace();
                   }
               }
            }
        });

       messageReceiver.start();

    }

    public static  ChatClient getInstance(int port,String address,String username){
           try{
               if (chatClientInstance == null){
                   chatClientInstance=new ChatClient(port,address,username);
               }
           }catch (Exception e){
               e.printStackTrace();
           }
           return  chatClientInstance;

    }


}
