package sample;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingDeque;

public class Client {

    private Socket clientSocket;
    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    private final LinkedBlockingDeque<String> MESSAGE;
    private final LinkedBlockingDeque<String> MESSAGE_BY_CLIENT;


    private String username="";

    public Client(String address,int port){
        MESSAGE=new LinkedBlockingDeque<>();
        MESSAGE_BY_CLIENT=new LinkedBlockingDeque<>();
        try {
            //Connection setup
            clientSocket=new Socket(address,port);
            inputStream=new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
            outputStream=new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader input=new BufferedReader(new InputStreamReader(System.in));

            //Contacting with a server
            String serverMessage=inputStream.readUTF();
            System.out.println(serverMessage);

            //Set client username
            setUsername(input.readLine());

            //send client's username to the server
            outputStream.writeUTF(username);

            //connection accepted or rejected
            serverMessage=inputStream.readUTF();
            System.out.println(serverMessage);



        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void setUsername(String username) {
        this.username = username;
    }




}
