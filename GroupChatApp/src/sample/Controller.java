package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    
    private static  final Controller MAIN_CONTROLLER=new Controller();
    private SingletonObservers chatObserver;






    @FXML
    private ListView clientsObserverList;

    @FXML
    private TextField messageField;
    @FXML
    private ListView messageContainer;



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
         chatObserver =SingletonObservers.getSingletonObservers();
        messageContainer.setItems(chatObserver.chatMessageObserver);

            clientsObserverList.setItems(chatObserver.clientUsernameObserver);
    }

    public void sendMessage(ActionEvent actionEvent) {
        String messageToSend=messageField.getText();
                LoginController.newClient.sendMessage(messageToSend);
                messageField.setText("");
    }



    public  static  Controller getInstance(){
        return  MAIN_CONTROLLER;
    }

    public void logOut(ActionEvent actionEvent) {
                    LoginController.newClient.sendMessage("Quit");
                    System.exit(0);
    }



}

